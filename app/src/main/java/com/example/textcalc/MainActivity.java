package com.example.textcalc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private int firstOperand;
    private int secondOperand;
    private EditText getFirstValue;
    private EditText getSecondValue;
    private int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getFirstValue = findViewById(R.id.firstOperand);

        getSecondValue = findViewById(R.id.secondOperand);
    }


    public void onClick(View view) {


        firstOperand = Integer.parseInt(getFirstValue.getText().toString());

        secondOperand = Integer.parseInt(getSecondValue.getText().toString());

        result = firstOperand + secondOperand;

        putKeysIntoIntent();


    }

    private void putKeysIntoIntent() {

        Intent intent = new Intent(this, ViewActivity.class);

        intent.putExtra("first", firstOperand);

        intent.putExtra("second", secondOperand);

        intent.putExtra("result", result);

        startActivity(intent);

    }

}